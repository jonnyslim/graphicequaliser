%C1634544
function varargout = Equaliser1634544(varargin)
% EQUALISER1634544 MATLAB code for Equaliser1634544.fig
%      EQUALISER1634544, by itself, creates a new EQUALISER1634544 or raises the existing
%      singleton*.
%
%      H = EQUALISER1634544 returns the handle to a new EQUALISER1634544 or the handle to
%      the existing singleton*.
%
%      EQUALISER1634544('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EQUALISER1634544.M with the given input arguments.
%
%      EQUALISER1634544('Property','Value',...) creates a new EQUALISER1634544 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Equaliser1634544_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Equaliser1634544_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Equaliser1634544

% Last Modified by GUIDE v2.5 12-Apr-2019 00:55:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Equaliser1634544_OpeningFcn, ...
                   'gui_OutputFcn',  @Equaliser1634544_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Equaliser1634544 is made visible.
function Equaliser1634544_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Equaliser1634544 (see VARARGIN)

% Choose default command line output for Equaliser1634544
vol = 3;
speed = 1;
set(handles.volumeSlider,'value',vol);
set(handles.speedSlider,'value',speed);
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Equaliser1634544 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Equaliser1634544_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in spectralEnvelope.
function spectralEnvelope_Callback(hObject, eventdata, handles)
% hObject    handle to spectralEnvelope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename2 handles.pathname2] = uigetfile({'*.wav'; '*.mp3'; '*.flac'; '*.mp4'}, 'File Selector');
handles.fullpathname2 = strcat(handles.pathname2, filename2);
[a2 fs2] = audioread(handles.fullpathname2);
a2 = a2(:,1);
a1 = handles.a(:,1);
fs1 = handles.fs;

specFS = max(fs1, fs2);
if fs1 > fs2
    a2 = resample(a2, fs1, fs2);
else 
    a1 = resample(a1, fs2, fs1);
end

len_a1 = length(a1);
len_a2 = length(a2);
if len_a1 > len_a2
    a1 = a1(1:len_a2);
else
    a2 = a2(1:len_a1);
end

len_w = 1024;
hop = len_w/4;
nfft = len_w;

[a1_stft f] = stft(a1, len_w, hop, nfft, specFS);
[a2_stft ~] = stft(a2, len_w, hop, nfft, specFS);

a1_stft_amp = abs(a1_stft);
for k = 1:size(a1_stft_amp, 2)
    a1_env(:, k) = smoothdata(a1_stft_amp(:, k));
end

a2_stft_amp = abs(a2_stft);
for k = 1:size(a2_stft_amp, 2)
    a2_env(:, k) = smoothdata(a2_stft_amp(:, k));
end

Spec_stft = a1_stft./a1_env.*a2_env;
Spec = istft(Spec_stft, len_w, hop, nfft, specFS);

sound(Spec, specFS);

% --- Executes on button press in wahwah.
function wahwah_Callback(hObject, eventdata, handles)
% hObject    handle to wahwah (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
wah_wah(handles.a, handles.fs);

% --- Executes on button press in flanger.
function flanger_Callback(hObject, eventdata, handles)
% hObject    handle to flanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
flanger(handles.a, handles.fs);

% --- Executes on button press in jazzPreset.
function jazzPreset_Callback(hObject, eventdata, handles)
% hObject    handle to jazzPreset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[d_32, d_64, d_125, d_250, d_500, d_1K, d_2K, d_4K, d_8K, d_16K] = deal(4, 3, 1.5, 2, -1.5, -1.5, 0, 1.5, 3, 4);
set(handles.slider_32,'value',d_32);
set(handles.slider_64,'value',d_64);
set(handles.slider_125,'value',d_125);
set(handles.slider_250,'value',d_250);
set(handles.slider_500,'value',d_500);
set(handles.slider_1K,'value',d_1K);
set(handles.slider_2K,'value',d_2K);
set(handles.slider_4K,'value',d_4K);
set(handles.slider_8K,'value',d_8K);
set(handles.slider_16K,'value',d_16K);

% --- Executes on button press in classicalPreset.
function classicalPreset_Callback(hObject, eventdata, handles)
% hObject    handle to classicalPreset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[d_32, d_64, d_125, d_250, d_500, d_1K, d_2K, d_4K, d_8K, d_16K] = deal(4.5, 4, 3, 2, -1.5, -1.5, 0, 2, 3.5, 4);
set(handles.slider_32,'value',d_32);
set(handles.slider_64,'value',d_64);
set(handles.slider_125,'value',d_125);
set(handles.slider_250,'value',d_250);
set(handles.slider_500,'value',d_500);
set(handles.slider_1K,'value',d_1K);
set(handles.slider_2K,'value',d_2K);
set(handles.slider_4K,'value',d_4K);
set(handles.slider_8K,'value',d_8K);
set(handles.slider_16K,'value',d_16K);

% --- Executes on button press in popPreset.
function popPreset_Callback(hObject, eventdata, handles)
% hObject    handle to popPreset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[d_32, d_64, d_125, d_250, d_500, d_1K, d_2K, d_4K, d_8K, d_16K] = deal(-1.5, -1, 0, 2, 4, 4, 2, 0, -1, -1.5);
set(handles.slider_32,'value',d_32);
set(handles.slider_64,'value',d_64);
set(handles.slider_125,'value',d_125);
set(handles.slider_250,'value',d_250);
set(handles.slider_500,'value',d_500);
set(handles.slider_1K,'value',d_1K);
set(handles.slider_2K,'value',d_2K);
set(handles.slider_4K,'value',d_4K);
set(handles.slider_8K,'value',d_8K);
set(handles.slider_16K,'value',d_16K);

% --- Executes on button press in rockPreset.
function rockPreset_Callback(hObject, eventdata, handles)
% hObject    handle to rockPreset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[d_32, d_64, d_125, d_250, d_500, d_1K, d_2K, d_4K, d_8K, d_16K] = deal(5, 4, 3, 1.5, -0.5, -1, 0.5, 2, 4, 4.5);
set(handles.slider_32,'value',d_32);
set(handles.slider_64,'value',d_64);
set(handles.slider_125,'value',d_125);
set(handles.slider_250,'value',d_250);
set(handles.slider_500,'value',d_500);
set(handles.slider_1K,'value',d_1K);
set(handles.slider_2K,'value',d_2K);
set(handles.slider_4K,'value',d_4K);
set(handles.slider_8K,'value',d_8K);
set(handles.slider_16K,'value',d_16K);

% --- Executes on button press in technoPreset.
function technoPreset_Callback(hObject, eventdata, handles)
% hObject    handle to technoPreset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[d_32, d_64, d_125, d_250, d_500, d_1K, d_2K, d_4K, d_8K, d_16K] = deal(4.5, 4, 1.5, 0, -2, 2, 1, 1.5, 4, 4.5);
set(handles.slider_32,'value',d_32);
set(handles.slider_64,'value',d_64);
set(handles.slider_125,'value',d_125);
set(handles.slider_250,'value',d_250);
set(handles.slider_500,'value',d_500);
set(handles.slider_1K,'value',d_1K);
set(handles.slider_2K,'value',d_2K);
set(handles.slider_4K,'value',d_4K);
set(handles.slider_8K,'value',d_8K);
set(handles.slider_16K,'value',d_16K);

% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
% hObject    handle to reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.slider_32,'value',0);
set(handles.slider_64,'value',0);
set(handles.slider_125,'value',0);
set(handles.slider_250,'value',0);
set(handles.slider_500,'value',0);
set(handles.slider_1K,'value',0);
set(handles.slider_2K,'value',0);
set(handles.slider_4K,'value',0);
set(handles.slider_8K,'value',0);
set(handles.slider_16K,'value',0);

% --- Executes on button press in playButton.
function playButton_Callback(hObject, eventdata, handles)
% hObject    handle to playButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global player;
clear sound;
handles.Volume = get(handles.volumeSlider , 'value');
handles.fn = handles.fs/2;

% 32 Hertz
[B32 A32] = peaking(get(handles.slider_32, 'value'), 31.5, (44-22)/31.5, handles.fs);
Filter32 = filter(B32, A32, handles.a);

% 64 Hertz
[B64 A64] = peaking(get(handles.slider_64, 'value'), 63, (88-44)/63, handles.fs);
Filter64 = filter(B64, A64, Filter32);

% 125 Hertz
[B125 A125] = peaking(get(handles.slider_125, 'value'), 125, (177-88)/125, handles.fs);
Filter125 = filter(B125, A125, Filter64);

% 250 Hertz
[B250 A250] = peaking(get(handles.slider_250, 'value'), 250, (355-177)/250, handles.fs);
Filter250 = filter(B250, A250, Filter125);

% 500 Hertz
[B500 A500] = peaking(get(handles.slider_500, 'value'), 500, (710-355)/500, handles.fs);
Filter500 = filter(B500, A500, Filter250);

% 1K Hertz
[B1K A1K] = peaking(get(handles.slider_1K, 'value'), 1000, (1420-710)/1000, handles.fs);
Filter1K = filter(B1K, A1K, Filter500);

% 2K Hertz
[B2K A2K] = peaking(get(handles.slider_2K, 'value'), 2000, (2840-1420)/2000, handles.fs);
Filter2K = filter(B2K, A2K, Filter1K);

% 4K Hertz
[B4K A4K] = peaking(get(handles.slider_4K, 'value'), 4000, (5680-2840)/4000, handles.fs);
Filter4K = filter(B4K, A4K, Filter2K);

% 8K Hertz
[B8K A8K] = peaking(get(handles.slider_8K, 'value'), 8000, (11360-5680)/8000, handles.fs);
Filter8K = filter(B8K, A8K, Filter4K);

% 16K Hertz
[B16K A16K] = shelving(get(handles.slider_16K, 'value'), 16000, handles.fs, (22720-11360)/16000, 'Treble_Shelf');
handles.aFiltered = filter(B16K, A16K, Filter8K);

sound(handles.Volume*handles.aFiltered, handles.fs*get(handles.speedSlider, 'value'));

b = (handles.Volume*handles.a)';
subplot(2,1,1);
plot(b(2, 1:length(b)-1));
axis([0 length(b) -2 2])

c = (handles.Volume*handles.aFiltered)';
subplot(2,1,2);
plot(c(2, 1:length(c)-1));
axis([0 length(b) -2 2])

% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear sound;

% --- Executes on slider movement.
function volumeSlider_Callback(hObject, eventdata, handles)
% hObject    handle to volumeSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function volumeSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to volumeSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function speedSlider_Callback(hObject, eventdata, handles)
% hObject    handle to speedSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function speedSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to speedSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_32_Callback(hObject, eventdata, handles)
% hObject    handle to slider_32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_32_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_64_Callback(hObject, eventdata, handles)
% hObject    handle to slider_64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_64_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_125_Callback(hObject, eventdata, handles)
% hObject    handle to slider_125 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_125_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_125 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_250_Callback(hObject, eventdata, handles)
% hObject    handle to slider_250 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_250_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_250 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_16K_Callback(hObject, eventdata, handles)
% hObject    handle to slider_16K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_16K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_16K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_8K_Callback(hObject, eventdata, handles)
% hObject    handle to slider_8K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_8K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_8K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_500_Callback(hObject, eventdata, handles)
% hObject    handle to slider_500 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_500_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_500 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_1K_Callback(hObject, eventdata, handles)
% hObject    handle to slider_1K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_1K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_1K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_2K_Callback(hObject, eventdata, handles)
% hObject    handle to slider_2K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_2K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_2K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_4K_Callback(hObject, eventdata, handles)
% hObject    handle to slider_4K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_4K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_4K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in browse.
function browse_Callback(hObject, eventdata, handles)
% hObject    handle to browse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename handles.pathname] = uigetfile({'*.wav'; '*.mp3'; '*.flac'; '*.mp4'}, 'File Selector');
[fpath fname fext] = fileparts(filename);
switch lower(fext)
    case '.wav'
        handles.fullpathname = strcat(handles.pathname, filename);
        [handles.a, handles.fs] = audioread(handles.fullpathname);
        set(handles.address, 'String', handles.fullpathname)
    case {'.mp3', '.flac', '.mp4'}
        temp = audioread(filename);
        info = audioinfo(filename);
        audiowrite('temp.wav', temp, info.SampleRate);
        [handles.a, handles.fs] = audioread('temp.wav');
        handles.originalpathname = strcat(handles.pathname, filename);
        handles.fullpathname = 'temp.wave';
        set(handles.address, 'String', handles.originalpathname)
    otherwise
        set(handles.address, 'String', 'Audio file requires extension of .wav, .mp3, .flac or .mp4')
end

guidata(hObject, handles)

% --- Executes on button press in loadPreset.
function loadPreset_Callback(hObject, eventdata, handles)
% hObject    handle to loadPreset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile('*.mat')
newfilename = fullfile(pathname, filename);
C = load(newfilename,'-ascii');
set(handles.slider1,'Value',C(1,1));
set(handles.slider2,'Value',C(1,2));
set(handles.slider12,'Value',C(1,3));
set(handles.slider13,'Value',C(1,4));
set(handles.slider14,'Value',C(1,5));
set(handles.slider15,'Value',C(1,6));
set(handles.slider16,'Value',C(1,7));
set(handles.slider17,'Value',C(1,8));
set(handles.slider18,'Value',C(1,9));
set(handles.slider19,'Value',C(1,10));

% --- Executes on button press in savePreset.
function savePreset_Callback(hObject, eventdata, handles)
% hObject    handle to savePreset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = zeros(1,10);
p(1,1) = get(handles.slider1,'Value');
p(1,2) = get(handles.slider2,'Value');
p(1,3) = get(handles.slider12,'Value');
p(1,4) = get(handles.slider13,'Value');
p(1,5) = get(handles.slider14,'Value');
p(1,6) = get(handles.slider15,'Value');
p(1,7) = get(handles.slider16,'Value');
p(1,8) = get(handles.slider17,'Value');
p(1,9) = get(handles.slider18,'Value');
p(1,10) = get(handles.slider19,'Value');
[filename, pathname] = uiputfile('*.mat','Save Workspace Variables As');
newfilename = fullfile(pathname, filename);
save(newfilename,'p','-ascii');
