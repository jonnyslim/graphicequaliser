This project is a matlab implementation of a Equaliser UI, where a user can upload a audio file and perform frequency modulation as well as other traditional musical effects.
The work is an example of a use for the Fourier Transform
