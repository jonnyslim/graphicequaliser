%C1634544
function [y] = flanger(x,Fs)
% parameters to vary the effect %
max_time_delay=0.003; % 3ms max delay in seconds
rate=1; %rate of flange in Hz

index=1:length(x);
sin_ref = (sin(2*pi*index*(rate/Fs)))';
max_samp_delay=round(max_time_delay*Fs);
y = zeros(length(x),1);
y(1:max_samp_delay)=x(1:max_samp_delay);
amp=0.7;
for i = (max_samp_delay+1):length(x),
cur_sin=abs(sin_ref(i));
cur_delay=ceil(cur_sin*max_samp_delay);
y(i) = (amp*x(i)) + amp*(x(i-cur_delay));
end
sound(y, Fs)
end